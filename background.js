function onError(error) {
    console.log(`Error: ${error}`);
}

function storeLastOpened() {
    var now = new Date();
    var setStorage = browser.storage.local.set({
        lastOpened: +new Date(now.getFullYear(), now.getMonth(), now.getDate())
    });
    setStorage.then(null, onError);
}

function getLastOpened(cb) {
    var lastOpened;
    var getStorage = browser.storage.local.get("lastOpened");
    getStorage.then(onGot, onError);

    function onGot(item) {
        lastOpened = item.lastOpened;
        console.log(lastOpened);
        cb(lastOpened)
    }
}

function openPajacyk() {
    var url = 'http://www.pajacyk.pl/';

    var querying = browser.tabs.query({url: url});
    querying.then(queriedTabs, onError);

    function queriedTabs(tabs) {
        if (tabs.length === 0) {
            browser.tabs.create({ url:url, active: false });
            storeLastOpened();
        }
        else {
            browser.tabs.update(tabs[0].id, { active: false });
            storeLastOpened();
        }
    };
}

function createAlarm() {
    var now = new Date();
    var day = now.getDate();

    if (now.getHours() >= 0) {
        var getStorage = browser.storage.local.get("lastOpened");
        getStorage.then(onGot, onError);

        function onGot(item) {
            var lastOpened = item.lastOpened;
            var now = new Date();
            var currentTimestamp = +new Date(now.getFullYear(), now.getMonth(), now.getDate());
            if (lastOpened < currentTimestamp || lastOpened === undefined) {
                openPajacyk();
            }
        }
        day += 1;
    }

    var timestamp = +new Date(now.getFullYear(), now.getMonth(), day, 0, 0, 0, 0);

    browser.alarms.create('12AM', {when: timestamp});
}


function alarmHandle(alarm) {
    if (alarm.name === '12AM') {
        openPajacyk();
        browser.alarms.create('12AM01', {delayInMinutes: 1});
    }
    if (alarm.name === '12AM01') {
        createAlarm()
    }
}

browser.alarms.onAlarm.addListener(alarmHandle);

createAlarm();
